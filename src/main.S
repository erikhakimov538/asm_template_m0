     .syntax	unified
	 .cpu cortex-m0

     .equ	GPIO_PORTA_BASE,  0x40080000
     .equ MDR_PORTA_BASE,   0x40080000
     // .equ Pin_6, 0x0040
     // .equ Pin_7,
     // .equ Pin_8,
     // .equ Pin_9,
     // .equ Pin_10,

     .text

     .thumb
     .thumb_func
     .align 2
     .globl   main_asm
     .type    main_asm, %function

main_asm:
/* включение порта А */
     ldr r0, =MDR_PORTA_BASE
     bl RST_CLK_PCLKcmd

/* конфигурации всех LED */
     ldr r0, =MDR_PORTA_BASE
     ldr r1, =6
     bl port_init
     ldr r1, =7
     bl port_init
     ldr r1, =8
     bl port_init
     ldr r1, =9
     bl port_init
     ldr r1, =10
     bl port_init


     ldr r0, =MDR_PORTA_BASE
     ldr r1, =6
loop_1:
     bl led_on_asm_mod
     push {r0, r1}
     ldr r0, =100000
     bl delay_asm
     pop {r0, r1}
     bl led_off_asm_mod
     push {r0, r1}
     ldr r0, =100000
     bl delay_asm
     pop {r0, r1}

     b	loop_1

    .end
