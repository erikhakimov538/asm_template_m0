INCLUDE(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
CMAKE_FORCE_C_COMPILER(arm-none-eabi-gcc GNU)

SET(CMAKE_OBJCOPY "arm-none-eabi-objcopy")
SET(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/src/MDR1986BE4.ld)
SET(COMMON_FLAGS "-mcpu=cortex-m0 -mthumb -mthumb-interwork -ffunction-sections -fdata-sections -Wall -Wextra -Wshadow  \
 -Wredundant-decls -Wno-missing-field-initializers -pipe")

SET(CMAKE_C_FLAGS "${COMMON_FLAGS} -std=gnu99")

SET(CMAKE_EXE_LINKER_FLAGS "-mcpu=cortex-m0 -Wl,-gc-sections -T ${LINKER_SCRIPT} -ffreestanding")

